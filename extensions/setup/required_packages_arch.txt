# Required packages
python2
lsb-release
openssh
curl
wget
git
ca-certificates
gzip
tar
sudo

# Required python packages
python2-yaml
python2-jinja
python2-httplib2
python2-boto
python2-docker
python2-pexpect

# Ansible
ansible
ansible-lint
