#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ROOT=$DIR/..
pushd $DIR
ansible-playbook -i $ROOT/production.ini -vvvv --check --step --vault-password-file $ROOT/.vpass $DIR/dockerhosts.yml
popd
# --step --check
